{% from "percona/map.jinja" import percona_settings with context %}
{% set os_family = salt['grains.get']('os_family') %}
{% set initsystem = salt['grains.get']('init') %}

mysql_python_dep:
  pkg.installed:
    - name: {{ percona_settings.python_mysql }}
    - reload_modules: True
