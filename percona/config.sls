{% from "percona/map.jinja" import percona_settings with context %}
{% set os_family = salt['grains.get']('os_family') %}

{{ percona_settings.config_directory }}:
  file.directory:
    - makedirs: True
    - user: root
    - group: root

include:
  - .service
  - .deps

{% if 'config' in percona_settings and percona_settings.config is mapping %}
{% set global_params= {} %}
{%   if 'my.cnf' in percona_settings.config %}
{%     do global_params.update(percona_settings.config['my.cnf'].get('mysqld',{})) %}
{%   endif %}
{%   for file, content in percona_settings.config|dictsort %}
{%     do global_params.update(percona_settings.config[file].get('mysqld',{})) if file != 'my.cnf' %}
{%     if file == 'my.cnf' %}
{%       set filepath = percona_settings.my_cnf_path %}
{%     else %}
{%       set filepath = percona_settings.config_directory + '/' + file %}
{%     endif %}
{{ filepath }}:
  file.managed:
    - user: root
    - group: root
    - mode: 0644
    - source: salt://percona/files/mysql.cnf.j2
    - template: jinja
    - context:
        config: {{ content |default({}) }}
    - require_in:
      - pkg: percona_client
      - pkg: percona_server
    - require:
      - file: {{ percona_settings.config_directory }}
{%     if percona_settings.reload_on_change %}
    - watch_in:
      - service: percona_svc
{%     endif %}
{%   endfor %}
{% endif %}

{% if percona_settings.datadir != '/var/lib/mysql' %}
percona-data-dir:
  file.directory:
    - name: {{ percona_settings.datadir }}
    - makedirs: True
    - user: {{ percona_settings.user }}
    - group: {{ percona_settings.group }}
    - mode: 0755
    - require_in:
      - pkg: percona_server
    - require:
      - user: {{ percona_settings.user }}
      - group: {{ percona_settings.group }}
{% endif %}

{% if percona_settings.binlogs_dir != '/var/lib/mysql' %}
percona-binlogs-dir:
  file.directory:
    - name: {{ percona_settings.binlogs_dir }}
    - makedirs: True
    - user: {{ percona_settings.user }}
    - group: {{ percona_settings.group }}
    - mode: 0755
    - require_in:
      - pkg: percona_server
    - require:
      - user: {{ percona_settings.user }}
      - group: {{ percona_settings.group }}
{% endif %}

{% if os_family == 'RedHat' %}
percona-config-d-dir:
  file.directory:
    - name: /etc/mysql/conf.d
    - makedirs: True
    - mode: 0755
    - require_in:
      - pkg: percona_server

percona-percona-server-conf-d-dir:
  file.directory:
    - name: /etc/mysql/percona-server.conf.d
    - makedirs: True
    - mode: 0755
    - require_in:
      - pkg: percona_server
{% endif %}

{% for global, value in global_params.iteritems() %}

{% if 'tmpdir' in global %}
{{ value }}:
  file.directory:
    - mode: 1777
    - makedirs: True
    - require_in:
      - pkg: percona_server
{% endif %}

{{ global }}:
  percona.setglobal:
    - value: {{ value }}
    - connection_pass: {{ percona_settings.root_password }}
    - fail_on_missing: False
    - fail_on_readonly: False
    - require:
      - service: percona_svc
      - pkg: mysql_python_dep
{% endfor %}
