#!jinja|yaml|gpg
# -*- coding: utf-8 -*-
# vim: ft: yaml:ts: 2:sw: 2
#
# Not all values listed here are considered as default-variables
#
mysql:
  root_password: milch

  db_users:
    streetlamp:
      state: present
      host: 127.0.0.1
      password: master1923
      database_grants:
        - database: 'streetdb.*'
          grant: ['all privileges']
          grant_option: False
    entries:
      state: present
      host: 127.0.0.1
      password: slave30923
      database_grants:
        - database: 'streetdb.*'
          grant: ['all privileges']
          grant_option: False
    test_user_localhost:
      username: test_user
      host: 127.0.0.1
      password: test123124
      database_grants:
        - database: 'streetdb.*'
          grant: ['all privileges']
          grant_option: False
    test_user_app:
      username: test_user
      host: "'app%.prod.foobar.deposit'"
      password: test123124
      database_grants:
        - database: 'streetdb.*'
          grant: ['all privileges']
          grant_option: False
  db_name:
    streetdb:
      state: present
      char: "utf8"
