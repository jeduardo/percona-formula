#!jinja|yaml|gpg
# -*- coding: utf-8 -*-
# vim: ft: yaml:ts: 2:sw: 2
#
# Not all values listed here are considered as default-variables
#
{% set total_memory = salt['grains.get']('mem_total') %}
{% set innodb_buffer_pool_size = ((((total_memory * 65)/100) * 1024) * 1024) | round | int %}
{% set server_id = salt['grains.get']('fqdn_ip4')[0] | replace('.','') %}

mysql:
  root_password: milch
  reload_on_change: True
  config:
    my.cnf:
      append:
        '!includedir /etc/mysql/conf.d/': no_param
        '!includedir /etc/mysql/percona-server.conf.d/': no_param
    server.cnf:
      client:
        port: 3306
        socket: /var/run/mysqld/mysqld.sock
      mysqld_safe:
        socket: /var/run/mysqld/mysqld.sock
        nice: 0
        syslog: no_param
        log-error: /var/log/mysql/error.log
        pid-file: /var/run/mysqld/mysqld.pid
      mysqld:
        bind_address: 0.0.0.0
        socket: /var/lib/mysql/mysql.sock
        pid-file: /var/run/mysqld/mysqld.pid
        datadir: /var/lib/mysql
        tmpdir: /tmp
        user: mysql
        max_allowed_packet: 67108864
        max_connections: 2000
        open_files_limit: 4000
        tmp_table_size: 67108864
        max_heap_table_size: 67108864
        default_storage_engine: InnoDB
        character-set-server: utf8
        skip_name_resolve: no_param
        query_cache_type: 0
        query_cache_size: 0
        performance_schema: ON
        userstat: 1
        server_id: {{ server_id }}
        log_bin: /var/lib/mysql/mysql-bin
        log_bin_index: /var/lib/mysql/mysql-bin.index
        binlog_format: ROW
        expire_logs_days: 2
        max_binlog_size: {{ 100 * 1024 * 1024 }}
        enforce_gtid_consistency: 1
        gtid_mode: 'on'
        log_slave_updates: no_param
        innodb_buffer_pool_size: {{ innodb_buffer_pool_size }}
        innodb_log_file_size: {{ 512 * 1024 * 1024 }}
        innodb_log_buffer_size: {{ 16 * 1024 * 1024 }}
        innodb_flush_log_at_trx_commit: 1
        innodb_flush_method: O_DIRECT
        sync_binlog: 1
        innodb_buffer_pool_instances: 8
        innodb_thread_concurrency: {{ grains['num_cpus'] * 2 }}
        innodb_io_capacity: 1000
        innodb_io_capacity_max: 3000
        innodb_buffer_pool_dump_pct: 75
        innodb_file_per_table: ON
        innodb_read_io_threads: {{ grains['num_cpus'] * 4 }}
        innodb_write_io_threads: {{ grains['num_cpus'] * 4 }}
        innodb_flush_neighbors: 0
        innodb_print_all_deadlocks: ON
        log-warnings: 2
        log-error : /var/log/mysql-error.log
        slow_query_log: 1

  db_users:
    root:
      state: present
      host: 127.0.0.1
      password: foobar
      database_grants:
        - database: '*.*'
          grant: ['all privileges']
          grant_option: False
    test:
      state: present
      host: 127.0.0.1
      password: test123
      database_grants:
        - database: '*.*'
          grant: ['all privileges']
          grant_option: False
